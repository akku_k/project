<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Laravel CRUD</title>

        <!-- bootstrap minified css -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <!-- bootstrap minified js -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        
        <!-- custom CSS -->
        
    </head>

    <body>
        <div class="container">
            <h1>CRUD</h1>
@yield('content')
        </div>
    </body>
</html>
